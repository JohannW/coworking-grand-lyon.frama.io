---
nom: Locaux Motiv'
logo: https://locauxmotiv.fr/wp-content/uploads/2021/11/Logo_LM-horizontal-transparent.png
photo: https://locauxmotiv.fr/wp-content/uploads/2021/11/20200728_150404-scaled.jpg
---

<!-- logo -->
![Locaux Motiv'](https://locauxmotiv.fr/wp-content/uploads/2021/11/Logo_LM-horizontal-transparent.png)
<!-- sous-titre -->
Locaux Motiv', un tiers lieu de l'ESS en gestion collective
> 🇬🇧 a third-place for the social economy stakeholder's

# Locaux Motiv'

<!-- Photo -->
![](https://locauxmotiv.fr/wp-content/uploads/2021/10/IMG_3407.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Locaux Motiv' est un Tiers-Lieu d'expérimentation, de partage et de diffusion de projet de l'ESSE (Economie Sociale Solidaire et Ecologique). Le projet rassemble à la fois associations, entreprises, personnes individuelles qui se reconnaissent dans des valeurs et des objectifs partagés :
- La gestion, l’animation et la promotion d’un Tiers-Lieu inclusif,
- La coordination, le portage et la diffusion de projets collectifs professionnels,
- La promotion et la contribution à la mise en réseau des acteurs de l’économie sociale, solidaire et écologique.


## Services
- Postes nomades, OpenSpace partagés, bureaux en propre
- Internet Fibre par câble ethernet ou wifi
- Salles de réunions
- Espace de stockage
- Espace reprographie
- Cuisine partagée et équipée
- Terrasse l'été
- Commandes groupées (pain, huiles d'olive, etc.)

## Localisation

Au coeur de la Guillotière (Lyon 7), sur la place Mazagran.
Accès sur https://locauxmotiv.fr/infos-pratiques/acces/

https://www.locauxmotiv.fr/
Pour nous contacter : www.locauxmotiv.fr
